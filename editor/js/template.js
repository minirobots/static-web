// vim: expandtab ts=2 sw=2 ai foldmethod=marker foldlevel=0

/**
 * Minirobots - Turtle Web Editor
 * https://minirobots.com.ar
 *
 * Author: Leo Vidarte <lvidarte@gmail.com>
 *
 * This is free software,
 * you can redistribute it and/or modify it
 * under the terms of the GPL version 3
 * as published by the Free Software Foundation.
 *
 */

if (typeof turtlepage == 'undefined') { var turtlepage = {}; }

turtlepage.getRandomColour = function() {
  let colours = config.colours.field_colours.concat(
    Object.values(config.colours.minirobots_colours).slice(0, 7)
  );
  return colours[
    Math.floor(Math.random() * config.colours.field_colours.length)
  ];
};

turtlepage.getMinirobotsColoursBlocks = function() {
  let blocks = '';
  for (const value of Object.values(config.colours.minirobots_colours)) {
    blocks += `<block type="colour_picker"><title name="COLOUR">${value}</title></block>\n`
  }

  return blocks;
};

turtlepage.start = function() {
  // HEADER {{{
  var header = `
    <div id="header">
        <div id="logo">
          <a href="/"><img src="minirobots.png" width="300"></a>
        </div>

        <div id="turtle-velocity">
          <img src="speed-white.png"/><br />
          <input type="range" id="speed" name="volume" min="1500" max="2000" value="1750" step="50" class="slider" title="Velocidad">

        </div>

        <div id="editor-buttons">
          <!-- <img id="spinner" src="loading.gif"/> -->

          <button id="runButton" title="Ejecutar programa" class="run" onclick="Turtle.runButtonClick();">▶ Ejecutar</button>
          <button id="stopButton" title="Parar la prueba" class="stop" onclick="Turtle.stopButtonClick();" style="display:none">■ Parar</button>
          <button id="clearButton" title="Borrar dibujo" class="clear" onclick="Turtle.clearButtonClick();" style="display:none">⌫ Borrar</button>

          <button id="sendButton" title="No hay un robot conectado" class="send" onclick="Turtle.sendButtonClick()" style="display:none">Enviar al Robot</button>
          <button id="cancelButton" title="Cancelar ejecución del programa" class="cancel" onclick="Turtle.cancelButtonClick()" style="display:none">Detener el Robot</button>

          <button id="canvas-increase-size" onclick="Turtle.canvasIncreaseSize()" title="Agrandar el tamaño del lienzo" class="emoji zoom">➕</button>
          <button id="canvas-decrease-size" onclick="Turtle.canvasDecreaseSize()" title="Achicar el tamaño del lienzo" class="emoji zoom">➖</button>

          <button onclick="Turtle.loadFile()" title="Cargar desde un archivo .minirobots" class="emoji file">📂</button>
          <button onclick="Turtle.saveFile()" title="Guardar en un archivo .minirobots" class="emoji file">💾</button>
          <button onclick="Turtle.setIP()" title="Configurar IP del robot" class="emoji settings">🔧</button>
          <input type="file" id="my_file" style="display:none" accept=".minirobots" />
        </div>

    </div>
  `;
  // }}}
  // EDITOR {{{
  var editor = `
    <div id="editor">      
      <table width="100%">
        <tr>               
          <td valign="top">
            <script type="text/javascript" src="blockly/blockly_compressed.js"></script>
            <script type="text/javascript" src="blockly/blocks_compressed.js"></script>
            <script type="text/javascript" src="blockly/msg/js/es.js"></script>
            <script type="text/javascript" src="blockly/generators/javascript.js"></script>
            <script type="text/javascript" src="blockly/generators/javascript/control.js"></script>
            <script type="text/javascript" src="blockly/generators/javascript/colour.js"></script>
            <script type="text/javascript" src="blockly/generators/javascript/logic.js"></script>
            <script type="text/javascript" src="blockly/generators/javascript/math.js"></script>
            <script type="text/javascript" src="blockly/generators/javascript/lists.js"></script>
            <script type="text/javascript" src="blockly/generators/javascript/variables.js"></script>
            <script type="text/javascript" src="blockly/generators/javascript/procedures.js"></script>
            <script type="text/javascript" src="blockly/generators/javascript/text.js"></script>
            <script type="text/javascript" src="js/common.js"></script>
            <script type="text/javascript" src="js/blocks.js"></script>
            <script type="text/javascript" src="js/file-saver.js"></script>
            ${turtlepage.toolbox()}
            <div id="blockly"></div>
          </td>
          <td valign="top" id="canvas">
            <table valign="top">
              <tr>
                <td>
                  <div id="canvas-container">
                    <canvas id="scratch" style="display:none"></canvas>
                    <canvas id="display"></canvas>
                    <textarea id="debugger" placeholder="Aquí verás la salida del bloque imprimir" style="display:inline" readonly></textarea>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </div>
  `;
  // }}}

  return header + editor;
};


turtlepage.toolbox = function() {
  var xml = `
    <xml id="toolbox" style="display:none">' 
      // TORTUGA {{{
      <category name="Tortuga" colour="${config.colours.minirobots_colours.turtle}">
        <block type="turtle_move">
          <field name="DIRECTION">forward</field>
          <value name="VALUE">
            <shadow type="math_number">
              <title name="NUM">100</title>
            </shadow>
          </value>
        </block>
        <block type="turtle_move">
          <field name="DIRECTION">backward</field>
          <value name="VALUE">
            <shadow type="math_number">
              <title name="NUM">100</title>
            </shadow>
          </value>
        </block>
        <block type="turtle_turn">
          <field name="DIRECTION">right</field>
          <value name="VALUE">
            <shadow type="angle_field">
              <title name="NAME">90</title>
            </shadow>
          </value>
        </block>
        <block type="turtle_turn">
          <field name="DIRECTION">left</field>
          <value name="VALUE">
            <shadow type="angle_field">
              <title name="NAME">90</title>
            </shadow>
          </value>
        </block>
        <block type="turtle_pen">
          <field name="DIRECTION">up</field>
        </block>
        <block type="turtle_pen">
          <field name="DIRECTION">down</field>
        </block>
        <block type="turtle_pen_colour">
          <value name="VALUE">
            <shadow type="colour_picker">
              <title name="COLOUR">${turtlepage.getRandomColour()}</title>
            </shadow>
          </value>
        </block>
        <block type="turtle_pen_width">
          <value name="VALUE">
            <shadow type="math_number">
              <title name="NUM">${config.turtle.line_width}</title>
            </shadow>
          </value>
        </block>
        <block type="turtle_speed">
          <field name="SPEED">1750</field>
        </block>
        <block type="turtle_visibility">
          <field name="ACTION">hide</field>
        </block>
        <block type="turtle_visibility">
          <field name="ACTION">show</field>
        </block>
        <block type="center_turtle"></block>
        <block type="led_colour_on">
          <value name="VALUE">
            <shadow type="colour_picker">
              <title name="COLOUR">${turtlepage.getRandomColour()}</title>
            </shadow>
          </value>
        </block>
        <block type="led_colour_off"></block>
        <block type="play_tone">
          <value name="VALUE">
            <shadow type="math_number">
              <title name="NUM">1</title>
            </shadow>
          </value>
        </block>
        <block type="sleep">
          <value name="VALUE">
            <shadow type="math_number">
              <title name="NUM">1</title>
            </shadow>
          </value>
        </block>
        <block type="reset_workspace"></block>
        <block type="canvas_size">
          <field name="SIZE">width</field>
        </block>
        <block type="canvas_size">
          <field name="SIZE">height</field>
        </block>
      </category>
      // }}}
      // BUCLES {{{
      <category name="Bucles" colour="${config.colours.minirobots_colours.loops_hue}">
        <block type="controls_repeat_ext">
          <value name="TIMES">
            <shadow type="math_number">
              <title name="NUM">4</title>
            </shadow>
          </value>
        </block>
        <block type="controls_whileUntil"></block>
        <block type="controls_for">
          <value name="FROM">
            <shadow type="math_number">
              <title name="NUM">1</title>
            </shadow>
          </value>
          <value name="TO">
            <shadow type="math_number">
              <title name="NUM">10</title>
            </shadow>
          </value>
          <value name="BY">
            <shadow type="math_number">
              <title name="NUM">1</title>
            </shadow>
          </value>
        </block>
        <block type="controls_forEach"></block>
        <block type="controls_flow_statements"></block>
      </category>
      // }}}
      // LOGIC {{{
      <category name="Lógica" colour="${config.colours.minirobots_colours.logic_hue}">
        <block type="controls_if"></block>
        <block type="logic_compare"></block>
        <block type="logic_operation"></block>
        <block type="logic_negate"></block>
        <block type="logic_boolean"></block>
        <block type="logic_null"></block>
        <block type="logic_ternary"></block>
      </category>
      // }}}
      // MATH {{{
      <category name="Matemática" colour="${config.colours.minirobots_colours.math_hue}">
        <block type="math_number"></block>
        <block type="math_arithmetic">
          <value name="A">
            <shadow type="math_number">
              <title name="NUM">1</title>
            </shadow>
          </value>
          <value name="B">
            <shadow type="math_number">
              <title name="NUM">1</title>
            </shadow>
          </value>
        </block>
        <block type="math_number_property">
          <value name="NUMBER_TO_CHECK">
            <shadow type="math_number">
              <title name="NUM">0</title>
            </shadow>
          </value>
        </block>
        <block type="math_round">
          <value name="NUM">
            <shadow type="math_number">
              <title name="NUM">3.14</title>
            </shadow>
          </value>
        </block>
        <block type="math_random_int">
          <value name="FROM">
            <shadow type="math_number">
              <title name="NUM">1</title>
            </shadow>
          </value>
          <value name="TO">
            <shadow type="math_number">
              <title name="NUM">100</title>
            </shadow>
          </value>
        </block>
        <block type="math_modulo">
          <value name="DIVIDEND">
            <shadow type="math_number">
              <title name="NUM">5</title>
            </shadow>
          </value>
          <value name="DIVISOR">
            <shadow type="math_number">
              <title name="NUM">2</title>
            </shadow>
          </value>
        </block>
        <block type="math_constrain">
          <value name="VALUE">
            <shadow type="math_number">
              <title name="NUM">500</title>
            </shadow>
          </value>
          <value name="LOW">
            <shadow type="math_number">
              <title name="NUM">1</title>
            </shadow>
          </value>
          <value name="HIGH">
            <shadow type="math_number">
              <title name="NUM">100</title>
            </shadow>
          </value>
        </block>
        <block type="math_random_float"></block>
        <block type="math_single"></block>
        <block type="math_trig"></block>
        <block type="math_constant"></block>
        <block type="math_on_list"></block>
      </category>
      // }}}
      // COLOURS {{{
      <category name="Colores" colour="${config.colours.minirobots_colours.colour_hue}">
        <block type="canvas_background_colour">
          <value name="VALUE">
            <shadow type="colour_picker">
              <title name="COLOUR">${turtlepage.getRandomColour()}</title>
            </shadow>
          </value>
        </block>
        <block type="colour_random"></block>
        <block type="colour_rgb">
          <value name="RED">
            <shadow type="math_number">
              <field name="NUM">100</field>
            </shadow>
          </value>
          <value name="GREEN">
            <shadow type="math_number">
              <field name="NUM">50</field>
            </shadow>
          </value>
          <value name="BLUE">
            <shadow type="math_number">
              <field name="NUM">0</field>
            </shadow>
          </value>
        </block>
        <block type="colour_blend">
          <value name="COLOUR1">
            <shadow type="colour_picker">
              <title name="COLOUR">${turtlepage.getRandomColour()}</title>
            </shadow>
          </value>
          <value name="COLOUR2">
            <shadow type="colour_picker">
              <title name="COLOUR">${turtlepage.getRandomColour()}</title>
            </shadow>
          </value>
          <value name="RATIO">
            <shadow type="math_number">
              <field name="NUM">0.5</field>
            </shadow>
          </value>
        </block>
        ${turtlepage.getMinirobotsColoursBlocks()}
      </category>
      // }}}
      // VARIABLES {{{
      <category name="Variables" colour="${config.colours.minirobots_colours.variables_hue}" custom="VARIABLE"></category>
      // }}}
      // LISTS {{{
      <category name="Listas" colour="${config.colours.minirobots_colours.lists_hue}">
        <block type="lists_create_with">
          <mutation items="0"></mutation>
        </block>
        <block type="lists_create_with"></block>
        <block type="lists_repeat">
          <value name="NUM">
            <shadow type="math_number">
              <title name="NUM">5</title>
            </shadow>
          </value>
        </block>
        <block type="lists_length"></block>
        <block type="lists_isEmpty">
          <value name="VALUE">
            <block type="variables_get">
              <field name="VAR"></field>
            </block>
          </value>
        </block>
        <block type="lists_indexOf">
          <value name="VALUE">
            <block type="variables_get">
              <field name="VAR"></field>
            </block>
          </value>
        </block>
        <block type="lists_getIndex">
          <value name="VALUE">
            <block type="variables_get">
              <field name="VAR"></field>
            </block>
          </value>
        </block>
        <block type="lists_setIndex">
          <value name="LIST">
            <block type="variables_get">
              <field name="VAR"></field>
            </block>
          </value>
        </block>
        <block type="lists_getSublist">
          <value name="LIST">
            <block type="variables_get">
              <field name="VAR"></field>
            </block>
          </value>
        </block>
        <block type="lists_split">
          <value name="DELIM">
            <shadow type="text">
              <field name="TEXT">,</field>
            </shadow>
          </value>
        </block>
        <block type="lists_sort"></block>
      </category>
      // }}}
      // PROCEDURE {{{
      <category name="Funciones" colour="${config.colours.minirobots_colours.procedures_hue}" custom="PROCEDURE"></category>
      // }}}
      // TEXT {{{
      <category name="Texto" colour="${config.colours.minirobots_colours.texts_hue}">
        <block type="text"></block>
        <block type="text_print">
          <value name="TEXT">
            <shadow type="text">
              <field name="TEXT"></field>
            </shadow>
          </value>
        </block>
        <block type="text_print_not_newline">
          <value name="TEXT">
            <shadow type="text">
              <field name="TEXT"></field>
            </shadow>
          </value>
        </block>
        <block type="text_print_clear">
          <value name="TEXT">
            <shadow type="text">
              <field name="TEXT"></field>
            </shadow>
          </value>
        </block>
        <block type="text_join"></block>
        <block type="text_append">
          <value name="TEXT">
            <shadow type="text"></shadow>
          </value>
        </block>
        <block type="text_prompt"></block>
        <block type="text_length">
          <value name="VALUE">
            <shadow type="text">
              <field name="TEXT">abc</field>
            </shadow>
          </value>
        </block>
        <block type="text_isEmpty">
          <value name="VALUE">
            <shadow type="text">
              <field name="TEXT"></field>
            </shadow>
          </value>
        </block>
        <!--
        <block type="text_indexOf">
          <value name="VALUE">
            <shadow type="variables_get">
              <field name="VAR">text</field>
            </shadow>
          </value>
          <value name="FIND">
            <shadow type="text">
              <field name="TEXT">abc</field>
            </shadow>
          </value>
        </block>
        <block type="text_charAt">
          <value name="VALUE">
            <shadow type="variables_get">
              <field name="VAR">text</field>
            </shadow>
          </value>
        </block>
        <block type="text_getSubstring">
          <value name="STRING">
            <shadow type="variables_get">
              <field name="VAR">text</field>
            </shadow>
          </value>
        </block>
        <block type="text_changeCase">
          <value name="TEXT">
            <shadow type="text">
              <field name="TEXT">abc</field>
            </shadow>
          </value>
        </block>
        -->
        <block type="text_trim">
          <value name="TEXT">
            <shadow type="text">
              <field name="TEXT">abc</field>
            </shadow>
          </value>
        </block>
        <block type="text_newline"></block>
      </category>
      // }}}
    </xml>
  `;

  return xml;
};
