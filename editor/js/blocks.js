// vim: expandtab ts=2 sw=2 ai foldmethod=marker foldlevel=0

/**
 * Minirobots - Turtle Web Editor
 * https://minirobots.com.ar
 *
 * Author: Leo Vidarte <lvidarte@gmail.com>
 *
 * This is free software,
 * you can redistribute it and/or modify it
 * under the terms of the GPL version 3
 * as published by the Free Software Foundation.
 *
 */

'use strict';

// Color override
Blockly.Msg.TURTLE = config.colours.minirobots_colours.turtle;
Blockly.Msg.LOOPS_HUE = config.colours.minirobots_colours.loops_hue;
Blockly.Msg.LOGIC_HUE = config.colours.minirobots_colours.logic_hue;
Blockly.Msg.MATH_HUE = config.colours.minirobots_colours.math_hue;
Blockly.Msg.COLOUR_HUE = config.colours.minirobots_colours.colour_hue;
Blockly.Msg.VARIABLES_HUE = config.colours.minirobots_colours.variables_hue;
Blockly.Msg.LISTS_HUE = config.colours.minirobots_colours.lists_hue;
Blockly.Msg.PROCEDURES_HUE = config.colours.minirobots_colours.procedures_hue;
Blockly.Msg.TEXTS_HUE = config.colours.minirobots_colours.texts_hue;

Blockly.FieldColour.COLOURS = config.colours.field_colours.concat(
  Object.values(config.colours.minirobots_colours).slice(0, 7)
);

const IGNORED_BY_ROBOT_MSG = "(esta instrucción es ignorada por el robot)";


// General blocks.
//
Blockly.Blocks['on_run'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(
          new Blockly.FieldImage(
            "minirobots-icon-transparent.png", 44, 44, {
              alt: "*",
              flipRtl: "FALSE"
            })
        );
    this.appendDummyInput()
        .appendField("Al ejecutar");
    this.setNextStatement(true, null);
    this.setColour(Blockly.Msg.VARIABLES_HUE);
    this.setTooltip(Blockly.Msg['ON_RUN_TOOLTIP']);
    this.setHelpUrl("");
  }
};

Blockly.JavaScript.on_run = function() {
  return '';
};

Blockly.Blocks.turtle_speed = {
  init: function() {
    this.setColour(Blockly.Msg.TURTLE);
    this.appendDummyInput()
      .appendField("dibujar con velocidad")
      .appendField(new Blockly.FieldDropdown([
        ["súper lenta", "1500"],
        ["lenta", "1625"],
        ["media", "1750"],
        ["rápida", "1875"],
        ["super rápida", "2000"]
      ]), "SPEED");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip(`Establece la velocidad de dibujo de la tortuga ${IGNORED_BY_ROBOT_MSG}`);
  }
};

Blockly.JavaScript.turtle_speed = function() {
  var speed = this.getFieldValue('SPEED');
  return 'Turtle.setSpeed(' + speed + ', \'' + this.id + '\');\n';
};

// @deprecated
Blockly.Blocks.move_forward = {
  init: function() {
    this.setColour(Blockly.Msg.TURTLE);
    this.appendValueInput('VALUE')
        .setCheck('Number')
        .appendField('avanzar');
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip('Mueve el robot hacia adelante');
  }
};

// @deprecated
Blockly.JavaScript.move_forward = function() {
  var value = Blockly.JavaScript.valueToCode(this, 'VALUE', Blockly.JavaScript.ORDER_NONE) || '0';
  return 'Turtle.moveForward(' + value + ', \'' + this.id + '\');\n';
};

// @deprecated
Blockly.Blocks.move_backward = {
  init: function() {
    this.setColour(Blockly.Msg.TURTLE);
    this.appendValueInput('VALUE')
        .setCheck('Number')
        .appendField('retroceder');
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip('Mueve el robot hacia atrás');
  }
};

// @deprecated
Blockly.JavaScript.move_backward = function() {
  var value = Blockly.JavaScript.valueToCode(this, 'VALUE', Blockly.JavaScript.ORDER_NONE) || '0';
  return 'Turtle.moveBackward(' + value + ', \'' + this.id + '\');\n';
};

Blockly.Blocks.turtle_move = {
  init: function() {
    this.setColour(Blockly.Msg.TURTLE);
    this.appendValueInput('VALUE')
        .setCheck('Number')
        .appendField(new Blockly.FieldDropdown(
          [['avanzar', 'forward'], ['retrocecer', 'backward']]
        ), 'DIRECTION');
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip('Avanzar o retoceder la tortuga');
  }
};

Blockly.JavaScript.turtle_move = function() {
  var direction = this.getFieldValue('DIRECTION');
  var value = Blockly.JavaScript.valueToCode(this, 'VALUE', Blockly.JavaScript.ORDER_NONE) || '0';
  return (direction == 'forward')
    ? 'Turtle.moveForward(' + value + ', \'' + this.id + '\');\n'
    : 'Turtle.moveBackward(' + value + ', \'' + this.id + '\');\n'
  ;
};

// @deprecated
Blockly.Blocks.turn_right = {
  init: function() {
    this.setColour(Blockly.Msg.TURTLE);
    this.appendValueInput('VALUE')
        .setCheck('Number')
        .appendField('girar a la derecha ↻');
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip('Gira el robot hacia la derecha');
  }
};

// @deprecated
Blockly.JavaScript.turn_right = function() {
  var value = Blockly.JavaScript.valueToCode(this, 'VALUE', Blockly.JavaScript.ORDER_NONE) || '0';
  return 'Turtle.turnRight(' + value + ', \'' + this.id + '\');\n';
};

// @deprecated
Blockly.Blocks.turn_left = {
  init: function() {
    this.setColour(Blockly.Msg.TURTLE);
    this.appendValueInput('VALUE')
        .setCheck('Number')
        .appendField('girar a la izquierda ↺');
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip('Gira el robot hacia la izquierda');
  }
};

// @deprecated
Blockly.JavaScript.turn_left = function() {
  var value = Blockly.JavaScript.valueToCode(this, 'VALUE', Blockly.JavaScript.ORDER_NONE) || '0';
  return 'Turtle.turnLeft(' + value + ', \'' + this.id + '\');\n';
};

Blockly.Blocks.turtle_turn = {
  init: function() {
    this.setColour(Blockly.Msg.TURTLE);
    this.appendValueInput('VALUE')
        .setCheck('Number')
        .appendField('girar a la ')
        .appendField(new Blockly.FieldDropdown(
          [['izquierda ↺', 'left'], ['derecha ↻', 'right']]
        ), 'DIRECTION');
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip('Girar la tortuga');
  }
};

Blockly.JavaScript.turtle_turn = function() {
  var direction = this.getFieldValue('DIRECTION');
  var value = Blockly.JavaScript.valueToCode(this, 'VALUE', Blockly.JavaScript.ORDER_NONE) || '0';
  return (direction == 'left')
    ? 'Turtle.turnLeft(' + value + ', \'' + this.id + '\');\n'
    : 'Turtle.turnRight(' + value + ', \'' + this.id + '\');\n'
  ;
};

// @deprecated
Blockly.Blocks.pen_down = {
  init: function() {
    this.setColour(Blockly.Msg.TURTLE);
    this.appendDummyInput()
        .appendField('bajar el lápiz')
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip('Baja el lápiz');
  }
};

// @deprecated
Blockly.JavaScript.pen_down = function() {
  return 'Turtle.penDown(\'' + this.id + '\');\n';
};

// @deprecated
Blockly.Blocks.pen_up = {
  init: function() {
    this.setColour(Blockly.Msg.TURTLE);
    this.appendDummyInput()
        .appendField('subir el lápiz')
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip('Sube el lápiz');
  }
};

// @deprecated
Blockly.JavaScript.pen_up = function() {
  return 'Turtle.penUp(\'' + this.id + '\');\n';
};

Blockly.Blocks.turtle_pen = {
  init: function() {
    this.setColour(Blockly.Msg.TURTLE);
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown(
          [['subir', 'up'], ['bajar', 'down']]
        ), 'DIRECTION')
        .appendField('el lápiz');
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip('Subir o bajar el lápiz de la tortuga');
  }
};

Blockly.JavaScript.turtle_pen = function() {
  var direction = this.getFieldValue('DIRECTION');
  return (direction == 'up')
    ? 'Turtle.penUp(\'' + this.id + '\');\n'
    : 'Turtle.penDown(\'' + this.id + '\');\n'
  ;
};

Blockly.Blocks.led_colour_on = {
  init: function() {
    this.setColour(Blockly.Msg.TURTLE);
    this.appendValueInput('VALUE')
        .setCheck('Colour')
        .appendField('encender')
        .appendField(new Blockly.FieldDropdown(
          [['ambos leds', '2'], ['el led derecho', '0'], ['el led izquierdo', '1']]
        ), 'LED')
        .appendField('con el color');
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip('Enciende los leds del robot');
  }
};

Blockly.JavaScript.led_colour_on = function() {
  var led   = this.getFieldValue('LED');
  var value = Blockly.JavaScript.valueToCode(this, 'VALUE', Blockly.JavaScript.ORDER_NONE) || '0';
  return 'Turtle.ledColour(' + led + ', ' + value + ', \'' + this.id + '\');\n';
};

Blockly.Blocks.led_colour_off = {
  init: function() {
    this.setColour(Blockly.Msg.TURTLE);
    this.appendDummyInput()
        .appendField('apagar')
        .appendField(new Blockly.FieldDropdown(
          [['ambos leds', '2'], ['el led derecho', '0'], ['el led izquierdo', '1']]
        ), 'LED');
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip('Apaga los leds del robot');
  }
};

Blockly.JavaScript.led_colour_off = function() {
  var led   = this.getFieldValue('LED');
  return 'Turtle.ledColour(' + led + ', "#000000", \'' + this.id + '\');\n';
};

Blockly.Blocks.play_tone = {
  init: function() {
    this.setColour(Blockly.Msg.TURTLE);
    this.appendValueInput('VALUE')
        .setCheck('Number')
        .appendField('tocar la nota')
        .appendField(new Blockly.FieldDropdown(config.turtle.tones), 'TONE')
        .appendField('durante (segundos)');
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip('Ejecuta la nota seleccionada');
  }
};

Blockly.JavaScript.play_tone = function() {
  var tone = this.getFieldValue('TONE');
  var sec  = Blockly.JavaScript.valueToCode(this, 'VALUE', Blockly.JavaScript.ORDER_NONE) || '0';
  return 'Turtle.playTone(' + tone + ', ' + sec + ', \'' + this.id + '\');\n';
};

Blockly.Blocks.sleep = {
  init: function() {
    this.setColour(Blockly.Msg.TURTLE);
    this.appendValueInput('VALUE')
        .setCheck('Number')
        .appendField('esperar (segundos)');
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip('Detiene la ejecución del robot durante durante el tiempo indicado');
  }
};

Blockly.JavaScript.sleep = function() {
  var value = Blockly.JavaScript.valueToCode(this, 'VALUE', Blockly.JavaScript.ORDER_NONE) || '0';
  return 'Turtle.sleep(' + value + ', \'' + this.id + '\');\n';
};

Blockly.Blocks['angle_field'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldAngle(90), "NAME");
    this.setOutput(true, null);
    this.setColour(Blockly.Msg.MATH_HUE);
    this.setTooltip(Blockly.Msg['ANGLE_FIELD_TOOLTIP']);
    this.setHelpUrl("");
  }
};

Blockly.JavaScript['angle_field'] = function(block) {
  var angle_name = block.getFieldValue('NAME');
  var code = angle_name;
  return [code, Blockly.JavaScript.ORDER_NONE];
};


Blockly.Blocks.reset_workspace = {
  init: function() {
    this.setColour(Blockly.Msg.TURTLE);
    this.appendDummyInput()
        .appendField('reiniciar');
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip(`Borra el lienzo y coloca la tortuga en el centro ${IGNORED_BY_ROBOT_MSG}`);
  }
};

Blockly.JavaScript.reset_workspace = function() {
  return 'Turtle.resetWorkspace(\'' + this.id + '\');\n';
};

Blockly.Blocks.turtle_pen_colour = {
  init: function() {
    this.setColour(Blockly.Msg.TURTLE);
    this.appendValueInput('VALUE')
        .setCheck('Colour')
        .appendField('dibujar con el color');
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip(`Establece el color del trazo del dibujo ${IGNORED_BY_ROBOT_MSG}`);
  }
};

Blockly.JavaScript.turtle_pen_colour = function() {
  var value = Blockly.JavaScript.valueToCode(this, 'VALUE', Blockly.JavaScript.ORDER_NONE) || '0';
  return 'Turtle.penColour(' + value + ', \'' + this.id + '\');\n';
};

Blockly.Blocks.turtle_pen_width = {
  init: function() {
    this.setColour(Blockly.Msg.TURTLE);
    this.appendValueInput('VALUE')
        .setCheck('Number')
        .appendField('dibujar con trazo');
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip(`Establece el ancho del trazo del dibujo ${IGNORED_BY_ROBOT_MSG}`);
  }
};

Blockly.JavaScript.turtle_pen_width = function() {
  var value = Blockly.JavaScript.valueToCode(this, 'VALUE', Blockly.JavaScript.ORDER_NONE) || '0';
  return 'Turtle.penWidth(' + value + ', \'' + this.id + '\');\n';
};

Blockly.Blocks.canvas_background_colour = {
  init: function() {
    this.setColour(Blockly.Msg.COLOUR_HUE);
    this.appendValueInput('VALUE')
        .setCheck('Colour')
        .appendField('pintar el lienzo de color');
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip(`Establece el color del lienzo ${IGNORED_BY_ROBOT_MSG}`);
  }
};

Blockly.JavaScript.canvas_background_colour = function() {
  var value = Blockly.JavaScript.valueToCode(this, 'VALUE', Blockly.JavaScript.ORDER_NONE) || '0';
  return 'Turtle.canvasBackgroundColour(' + value + ', \'' + this.id + '\');\n';
};

// @deprecated
Blockly.Blocks.hide_turtle = {
  init: function() {
    this.setColour(Blockly.Msg.TURTLE);
    this.appendDummyInput()
        .appendField('ocultar la tortuga')
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip(`Oculta la tortuga ${IGNORED_BY_ROBOT_MSG}`);
  }
};

// @deprecated
Blockly.JavaScript.hide_turtle = function() {
  return 'Turtle.hide(\'' + this.id + '\');\n';
};

// @deprecated
Blockly.Blocks.show_turtle = {
  init: function() {
    this.setColour(Blockly.Msg.TURTLE);
    this.appendDummyInput()
        .appendField('mostrar la tortuga')
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip(`Muestra la tortuga ${IGNORED_BY_ROBOT_MSG}`);
  }
};

// @deprecated
Blockly.JavaScript.show_turtle = function() {
  return 'Turtle.show(\'' + this.id + '\');\n';
};

Blockly.Blocks.turtle_visibility = {
  init: function() {
    this.setColour(Blockly.Msg.TURTLE);
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown(
          [['mostrar', 'show'], ['ocultar', 'hide']]
        ), 'ACTION')
        .appendField('la tortuga');
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip(`Muestra u oculta la tortuga ${IGNORED_BY_ROBOT_MSG}`);
  }
};

Blockly.JavaScript.turtle_visibility = function() {
  var action = this.getFieldValue('ACTION');
  return (action == 'show')
    ? 'Turtle.show(\'' + this.id + '\');\n'
    : 'Turtle.hide(\'' + this.id + '\');\n'
  ;
};

Blockly.Blocks.center_turtle = {
  init: function() {
    this.setColour(Blockly.Msg.TURTLE);
    this.appendDummyInput()
        .appendField('centrar la tortuga')
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip(`Centra la tortuga ${IGNORED_BY_ROBOT_MSG}`);
  }
};

Blockly.JavaScript.center_turtle = function() {
  return 'Turtle.center(\'' + this.id + '\');\n';
};

Blockly.Blocks.text_newline = {
  init: function() {
    this.setColour(Blockly.Msg.TEXTS_HUE);
    this.appendDummyInput()
        .appendField('salto de línea');
    this.setOutput(true, 'String');
    this.setTooltip(`Devuelve un caracter de salto de línea ${IGNORED_BY_ROBOT_MSG}`);
  }
};

Blockly.JavaScript.text_newline = function() {
  var code = '"\\n"';
  return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks.text_print_clear = {
  init: function() {
    this.setColour(Blockly.Msg.TEXTS_HUE);
    this.appendValueInput("TEXT")
        .setCheck(null)
        .appendField('borrar e imprimir');
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip(Blockly.Msg['TEXT_PRINT_TOOLTIP']);
    this.setHelpUrl(Blockly.Msg['TEXT_PRINT_HELPURL']);
  }
};

Blockly.JavaScript.text_print_clear = function(block) {
  var msg = Blockly.JavaScript.valueToCode(block, 'TEXT',
      Blockly.JavaScript.ORDER_NONE) || '\'\'';
  msg = msg.replace(/\\\\n/g, '\\n');
  return 'Turtle.debuggerClear(' + msg + ', \'' + this.id + '\');\n';
};

// @deprecated
Blockly.Blocks.canvas_width = {
  init: function() {
    this.setColour(Blockly.Msg.TURTLE);
    this.appendDummyInput()
        .appendField('ancho del lienzo');
    this.setOutput(true, 'Number');
    this.setTooltip(`Devuelve el ancho del lienzo ${IGNORED_BY_ROBOT_MSG}`);
  }
};

// @deprecated
Blockly.JavaScript.canvas_width = function() {
  var code = Turtle.canvasWidth;
  return [code, Blockly.JavaScript.ORDER_NONE];
};

// @deprecated
Blockly.Blocks.canvas_height = {
  init: function() {
    this.setColour(Blockly.Msg.TURTLE);
    this.appendDummyInput()
        .appendField('alto del lienzo');
    this.setOutput(true, 'Number');
    this.setTooltip(`Devuelve el alto del lienzo ${IGNORED_BY_ROBOT_MSG}`);
  }
};

// @deprecated
Blockly.JavaScript.canvas_height = function() {
  var code = Turtle.canvasHeight;
  return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks.canvas_size = {
  init: function() {
    this.setColour(Blockly.Msg.TURTLE);
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown(
          [['ancho', 'width'], ['alto', 'height']]
        ), 'SIZE')
        .appendField('del lienzo');
    this.setOutput(true, 'Number');
    this.setTooltip(`Devuelve el ancho/alto del lienzo ${IGNORED_BY_ROBOT_MSG}`);
  }
};

Blockly.JavaScript.canvas_size = function() {
  var size = this.getFieldValue('SIZE');
  var code = (size == 'width') ? Turtle.canvasWidth : Turtle.canvasHeight;
  return [code, Blockly.JavaScript.ORDER_NONE];
};

