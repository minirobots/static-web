// vim: expandtab ts=2 sw=2 ai foldmethod=marker foldlevel=0

/**
 * Minirobots - Turtle Web Editor
 * https://minirobots.com.ar
 *
 * Author: Leo Vidarte <lvidarte@gmail.com>
 *
 * This is free software,
 * you can redistribute it and/or modify it
 * under the terms of the GPL version 3
 * as published by the Free Software Foundation.
 *
 */

'use strict';

var config = {};

config.api_protocol = 'http';
config.api_minirobots = 'api.minirobots.com.ar';
config.ip = '';

config.colours = {
  // FIELD COLOURS {{{
  'field_colours': [
    '#ffffff', '#cccccc', '#c0c0c0', '#999999', '#666666', '#333333', '#000000',
    '#ffcccc', '#ff6666', '#ff0000', '#dd0000', '#bb0000', '#990000', '#770000',
    '#ffcc99', '#ff9966', '#ff9900', '#dd6600', '#bb6600', '#bb3300', '#993300',
    '#ffff99', '#ffff66', '#ffcc66', '#ffcc33', '#dd9933', '#ff9900', '#993333',
    '#ffffcc', '#ffff33', '#ffff00', '#ffcc00', '#aaaa00', '#999900', '#666600',
    '#99ff99', '#66ff99', '#33ff33', '#00ff00', '#33cc00', '#00bb00', '#009900',
    '#99ffff', '#33ffff', '#66cccc', '#00cccc', '#33aaaa', '#338888', '#336666',
    '#ccffff', '#66ffff', '#33ccff', '#3366ff', '#3333ff', '#0000bb', '#000099',
    '#ccccff', '#9999ff', '#6666cc', '#6633ff', '#6600cc', '#6633aa', '#333399',
    '#ffccff', '#ff99ff', '#cc66cc', '#cc33cc', '#aa33aa', '#883388', '#663366',
  ],
  // }}}
  // MINIROBOTS COLOURS {{{
  'minirobots_colours': {
    'turtle': '#2196F3',
    'loops_hue': '#ED1164',
    'logic_hue': '#A429AE',
    'math_hue': '#FFC60A',
    'colour_hue': '#A0783F',
    'variables_hue': '#38B549',
    'lists_hue': '#845EC2',
    'procedures_hue': '#004EB6',
    'texts_hue': '#FF9400',
  },
  // }}}
};

config.turtle = {};
config.turtle.line_width = 3;
config.turtle.tones = [
  // TONES {{{
  ['Do 1'      ,  '262'],
  ['Do♯/Re♭ 1' ,  '277'],
  ['Re 1'      ,  '294'],
  ['Re♯/Mi♭ 1' ,  '311'],
  ['Mi 1'      ,  '330'],
  ['Fa 1'      ,  '349'],
  ['Fa♯/Sol♭ 1',  '370'],
  ['Sol 1'     ,  '392'],
  ['Sol♯/La♭ 1',  '415'],
  ['La 1'      ,  '440'],
  ['La♯/Si♭ 1' ,  '466'],
  ['Si 1'      ,  '494'],
  ['Do 2'      ,  '523'],
  ['Do♯/Re♭ 2' ,  '554'],
  ['Re 2'      ,  '587'],
  ['Re♯/Mi♭ 2' ,  '622'],
  ['Mi 2'      ,  '659'],
  ['Fa 2'      ,  '698'],
  ['Fa♯/Sol♭ 2',  '740'],
  ['Sol 2'     ,  '784'],
  ['Sol♯/La♭ 2',  '831'],
  ['La 2'      ,  '880'],
  ['La♯/Si♭ 2' ,  '932'],
  ['Si 2'      ,  '988'],
  ['Do 3'      , '1047'],
  ['Do♯/Re♭ 3' , '1109'],
  ['Re 3'      , '1175'],
  ['Re♯/Mi♭ 3' , '1245'],
  ['Mi 3'      , '1319'],
  ['Fa 3'      , '1397'],
  ['Fa♯/Sol♭ 3', '1480'],
  ['Sol 3'     , '1568'],
  ['Sol♯/La♭ 3', '1661'],
  ['La 3'      , '1760'],
  ['La♯/Si♭ 3' , '1865'],
  ['Si 3'      , '1976'],
  ['Do 4'      , '2093'],
  ['Do♯/Re♭ 4' , '2217'],
  ['Re 4'      , '2349'],
  ['Re♯/Mi♭ 4' , '2489'],
  ['Mi 4'      , '2637'],
  ['Fa 4'      , '2794'],
  ['Fa♯/Sol♭ 4', '2960'],
  ['Sol 4'     , '3136'],
  ['Sol♯/La♭ 4', '3322'],
  ['La 4'      , '3520'],
  ['La♯/Si♭ 4' , '3729'],
  ['Si 4'      , '3951'],
  ['Do 5'      , '4186'],
  ['Do♯/Re♭ 5' , '4435'],
  ['Re 5'      , '4699'],
  ['Re♯/Mi♭ 5' , '4978']
  // }}}
];
